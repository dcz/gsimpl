# -*- Mode: Python; py-indent-offset: 4 -*-
# vim: tabstop=4 shiftwidth=4 expandtab
# SPDX-License-Identifier: LGPL-2.1
# Copyright Pygobject contributors
# Modified by gnome@dorotac.eu

from dataclasses import dataclass
import os

from gi.repository import GLib
from gi.repository import Gio


def dbg(v):
    print(repr(v))
    return v


def load_introspection_xml(name: str):
    data_dirs = os.getenv("XDG_DATA_DIRS", "/usr/share").split(":")
    for data_dir in data_dirs:
        try:
            #print(f"{data_dir}/dbus-1/interfaces/{name}.xml")
            with open(f"{data_dir}/dbus-1/interfaces/{name}.xml") as f:
                introspection = f.read()
            break
        except FileNotFoundError:
            pass
    else:
        raise FileNotFoundError(f"Unable to find portal definition {name}")
    return introspection


GLOBALSHORTCUTS = "org.freedesktop.impl.portal.GlobalShortcuts"
REBIND = "org.gnome.GlobalShortcutsRebind"
SESSION = "org.freedesktop.impl.portal.Session"
SHELL = 'org.gnome.Shell'

gs_xml = load_introspection_xml(GLOBALSHORTCUTS)
session_xml = load_introspection_xml(SESSION)
request_xml = load_introspection_xml("org.freedesktop.impl.portal.Request")
rebind_xml = load_introspection_xml(REBIND)

@dataclass
class SessionInfo:
    dbus_peer: str
    bus_id: int
    app_id: str
    bound: str


OK = 0
CANCELLED = 1
FAIL = 2
NOT_BOUND = "Not bound"


def xdgToMutter(trigger):
    """ The dbus API encodes triggers in the XDG shortcuts format,
    but mutter has its own with angle brackets.
    """
    parts = trigger.split('+');
    modifiers = ["<" + part + ">" for part in parts[:-1]]
    return ''.join(modifiers) + parts[-1]


def to_str(s):
     return GLib.Variant("s", s)

def to_strv(s):
     return GLib.Variant("as", s)
 
def key_to_str(d):
    return dict((k, to_str(v)) for k, v in d.items())

def describe_triggers(info):
    ginfo = dict(info)
    del ginfo['trigger_description']
    ginfo = key_to_str(ginfo)
    desc = ' or '.join(info.get('trigger_description', [])) or 'Disabled'
    ginfo['trigger_description'] = GLib.Variant("s", desc)
    return ginfo


def encode_asasv(shortcuts, transform=key_to_str):
    return [(name, transform(s)) for (name, s) in shortcuts]



class GlobalShortcutsImpl:
    def __init__(self):
        self.sessions = {}
        
        self.bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)

        self.settings_proxy = Gio.DBusProxy.new_sync(self.bus,
                                        Gio.DBusProxyFlags.NONE, None,
                                        'org.gnome.Settings.GlobalShortcutsProvider',
                                        '/org/gnome/Settings/GlobalShortcutsProvider',
                                        'org.gnome.Settings.GlobalShortcutsProvider',
                                        None)
        
        self.shell_proxy = Gio.DBusProxy.new_sync(self.bus,
                                        Gio.DBusProxyFlags.NONE, None,
                                        'org.gnome.Shell',
                                        '/org/gnome/Shell',
                                        SHELL,
                                        None)
        
        self.sender_checker = DBusSenderChecker([
            "org.freedesktop.portal.Desktop",
            "org.gnome.Settings",
            "org.gnome.Settings.GlobalShortcutsProvider",
            "org.gnome.Shell",
        ])

    def run_server(self):
        self.server_object = None
        self.rebind_server_object = None
        self.loop = GLib.MainLoop()

        def on_name_acquired(bus, name):
            pass

        owner_id = Gio.bus_own_name(Gio.BusType.SESSION,
                                    "org.freedesktop.impl.portal.desktop.gnome.GlobalShortcuts",
                                    Gio.BusNameOwnerFlags.NONE,
                                    self.on_bus_acquired,
                                    on_name_acquired,
                                    self.on_name_lost)
        
        try:
            self.loop.run()
        finally:
            Gio.bus_unown_name(owner_id)
            if self.server_object:
                self.bus.unregister_object(self.server_object)
            if self.rebind_server_object:
                self.bus.unregister_object(self.rebind_server_object)

    def on_name_lost(self, _bus, name):
        self.loop.quit()

    def on_bus_acquired(self, bus, name):
        self.server_object = bus.register_object("/org/freedesktop/portal/desktop",
                                          Gio.DBusNodeInfo.new_for_xml(gs_xml).interfaces[0],
                                          self.on_incoming_method_call,
                                          None,
                                          None)
        
        self.rebind_server_object = bus.register_object("/org/gnome/globalshortcuts",
                                          Gio.DBusNodeInfo.new_for_xml(rebind_xml).interfaces[0],
                                          self.on_incoming_method_call,
                                          None,
                                          None)
        
        bus.signal_subscribe('org.gnome.Shell',
                                        SHELL,
                                        None,
                                        '/org/gnome/Shell',
                                        None,
                                        Gio.DBusSignalFlags.NONE,
                                        self.on_incoming_signal)

    def on_incoming_method_call(self, bus, sender, object_path, interface_name, method_name, parameters, invocation):
        print(object_path, interface_name, method_name)

        try:
            self.sender_checker.checkInvocation(invocation)
        except Exception as e:
            msg = f"Access denied to {invocation.get_sender()}"
            print(msg)
            invocation.return_error_literal(
                    e.code, e.code, msg)
            return
        
        if (interface_name, method_name) == (GLOBALSHORTCUTS, "CreateSession"):
            handle, session_handle, app_id, options = parameters
            session_object = bus.register_object(session_handle,
                                        Gio.DBusNodeInfo.new_for_xml(session_xml).interfaces[0],
                                        self.on_incoming_method_call,
                                        None,
                                        None)
            self.sessions[session_handle] = SessionInfo(dbus_peer=sender,
                                                        app_id=app_id, bound=NOT_BOUND, bus_id=session_object)
            
            invocation.return_value(GLib.Variant("(ua{sv})", (OK, {"session_id": GLib.Variant("s", session_handle)})))
        elif (interface_name, method_name) == (GLOBALSHORTCUTS, "BindShortcuts"):
            self.handle_bind_shortcuts(invocation, *parameters)
        elif (interface_name, method_name) == (GLOBALSHORTCUTS, "ListShortcuts"):
            handle, session_handle = parameters
            actions = self.shell_proxy.ListShortcuts("(s)",
            session_handle)
            shortcuts = actions
            shortcuts = GLib.Variant("a(sa{sv})", encode_asasv(actions['shortcuts'], describe_triggers))
            invocation.return_value(GLib.Variant("(ua{sv})", (OK, {"shortcuts": shortcuts})))
        elif (interface_name, method_name) == (REBIND, "RebindShortcuts"):
            app_id, new_shortcuts = parameters
            sessions = [(handle, data) for handle, data in self.sessions.items() if data.app_id == app_id]
            for handle, info in sessions:
                def serialize_for_shell(name, s):
                    shortcut = {'shortcuts': to_strv(s['shortcuts'])}
                    desc = s.get('description', None)
                    if desc is not None:
                        shortcut['description'] = GLib.Variant('s', desc)
                    return shortcut
                
                new_shortcuts = [(name, serialize_for_shell(name, shortcut)) for name, shortcut in new_shortcuts]
                (final, changed) = self.shell_proxy.BindShortcuts("(sa(sa{sv}))", handle, new_shortcuts)
                final = final['shortcuts']
                if changed:
                    self.bus.emit_signal(
                        info.dbus_peer,
                        "/org/freedesktop/portal/desktop",
                        GLOBALSHORTCUTS,
                        "ShortcutsChanged",
                        GLib.Variant(
                            "(oa(sa{sv}))",
                            (handle, encode_asasv(final, describe_triggers))
                        )
                    )

            invocation.return_value(GLib.Variant("()", None))
        elif (interface_name, method_name) == (SESSION, "Close"):
            session_handle = object_path

            self.shell_proxy.BindShortcuts(
                "(sa(sa{sv}))",
                session_handle,
                [],
            )

            self.bus.unregister_object(self.sessions[session_handle].bus_id)
            
            invocation.return_value(GLib.Variant("()", None))
        else:
            invocation.return_error_literal(Gio.DBusError.NOT_SUPPORTED,
                    Gio.DBusError.NOT_SUPPORTED,
                    "No such method")

    def handle_bind_shortcuts(self, invocation, handle, session_handle, shortcuts, parent_window, options):
        def ret(response, results):
            invocation.return_value(GLib.Variant("(ua{sv})", (response, results)))

        session_info = self.sessions.get(session_handle, None)
        if session_info is None:
            return ret(FAIL, {})
        if session_info.bound != NOT_BOUND:
            return ret(FAIL, {})
        # TODO: possible race condition where "bound" is not set yet and another call comes?
        session_info.bound = "Bound"
        
        app_id = session_info.app_id
        def call_done(obj, result, params):
            self.handle_bind_shortcuts_settings_done(invocation, session_handle, obj, result, shortcuts)
          
        def translate_trigger(shortcut):
            shotcut = dict(shortcut) # don't modify in place
            if 'preferred_trigger' in shortcut:
                shortcut['preferred_trigger'] = xdgToMutter(shortcut['preferred_trigger'])
            return shortcut

        shortcuts = encode_asasv(
            [(name, translate_trigger(info)) for name, info in shortcuts]
        )
    
        self.settings_proxy.call('BindShortcuts',
                             GLib.Variant("(sa(sa{sv}))", (app_id, shortcuts)),#, parent_window),
                             Gio.DBusCallFlags.NONE,
                             500*3600,
                             None,
                             call_done,
                             None)
        
    def handle_bind_shortcuts_settings_done(self, invocation, session_handle, obj, result, shortcuts):
        try:
            (results, ) = obj.call_finish(result)
        except GLib.Error as e:
            invocation.return_value(GLib.Variant("(ua{sv})", (CANCELLED, {})))
            return

        descriptions = dict((name, info.get('description', None)) for name, info in shortcuts)
        
        def serialize_for_shell(name, s):
            shortcut = {'shortcuts': to_strv(s['shortcuts'])}
            desc = descriptions.get(name, None)
            if desc is not None:
                shortcut['description'] = desc
            return shortcut
        
        shortcuts = [(name, serialize_for_shell(name, shortcut)) for name, shortcut in results]

        actions, _removed = self.shell_proxy.BindShortcuts(
            "(sa(sa{sv}))",
            session_handle,
            shortcuts,
        )

        shortcuts = GLib.Variant("a(sa{sv})", encode_asasv(actions['shortcuts'], transform=describe_triggers))

        invocation.return_value(GLib.Variant("(ua{sv})", (OK, {"shortcuts": shortcuts})))

    def on_incoming_signal(self, conn_, sender, object_path, interface_name, signal_name, parameters):
        print(object_path, interface_name, signal_name, parameters)
        if not self.sender_checker._isSenderAllowed(sender):
            msg = f"Access denied to {sender}"
            print(msg)
            return
        
        if (interface_name, signal_name) == (SHELL, "Activated"):
            self.handle_activation(signal_name, *parameters)
        elif (interface_name, signal_name) == (SHELL, "Deactivated"):
            self.handle_activation(signal_name, *parameters)
            
    def handle_activation(self, signal_name, session_handle, trigger_name, timestamp):
        info = self.sessions.get(session_handle, None)
        if info is None:
            print("Activated invalid session", session_handle)
            return
        
        self.bus.emit_signal(info.dbus_peer,
                        "/org/freedesktop/portal/desktop",
                        GLOBALSHORTCUTS,
                        signal_name,
                        GLib.Variant("(osta{sv})",
                            (session_handle, trigger_name, timestamp, {})))


class DBusSenderChecker:
    """Copied from JS with async removed"""
    def __init__(self, allowList):
        """
        * @param {string[]} allowList - list of allowed well-known names
        """
        self._allowlistMap = {}

        self._uninitializedNames = set(allowList)

        watchList = []
        
        def on_appear(conn_, name, owner):
            if watched_name not in allowList:
                return
            self._allowlistMap[name] = owner
            self._checkAndResolveInitialized(name)

        def on_vanish(conn_, name):
            if watched_name not in allowList:
                return
            self._allowlistMap.pop(name, None)
            self._checkAndResolveInitialized(name)
        
        for watched_name in allowList:
            watchList.append(Gio.bus_watch_name(Gio.BusType.SESSION,
                watched_name,
                Gio.BusNameWatcherFlags.NONE,
                on_appear,
                on_vanish))
        self.watchList = watchList

    def _checkAndResolveInitialized(self, name):
        """
        @param {string} name - bus name for which the watcher got initialized
        """
        self._uninitializedNames.discard(name)

    def _isSenderAllowed(self, sender):
        """
        * @param {string} sender - the bus name that invoked the checked method
        * @returns {bool}
        """
        if sender in self._allowlistMap.values():
            return True
        else:
            if self._uninitializedNames:
                return None # Can't determine
            return False

    def checkInvocation(self, invocation):
        """
        * Check whether the bus name that invoked @invocation maps
        * to an entry in the allow list.
        *
        * @param {Gio.DBusMethodInvocation} invocation - the invocation
        * @returns {void}
        """
        if self._isSenderAllowed(invocation.get_sender()):
            return
        
        raise GLib.Error(Gio.DBusError,
            f'{invocation.get_method_name()} is not allowed',
            Gio.DBusError.ACCESS_DENIED)

    def __del__(self):
        for id in self._watchList:
            Gio.dbus_unwatch_name(id)
        self._watchList = []


GlobalShortcutsImpl().run_server()