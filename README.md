Gsimpl
=====

Implements the [Global Shortcuts](https://flatpak.github.io/xdg-desktop-portal/docs/doc-org.freedesktop.impl.portal.GlobalShortcuts.html) portal entry for the GNOME environment.

Why
-----

Because I'm too dump to write C in xdg-desktop-portal-gnome. This prototype can be rewritten into C later, but if I had to start with C I'd never have finished. So much string/array/dict processing!

Usage
-------

Gsimpl sits in between various components, so you need to get the right versions of Mutter, Gnome Shell, and Settings.

Before you run anything else, you need to have a dbus session running. My choice is to start mutter with `dbus-run-session sh run_mutter.sh`.

This is `run_mutter.sh`:

```
echo $DBUS_SESSION_BUS_ADDRESS > DBUSADDR
META_DEBUG_KEYBINDINGS=1 XDG_DESKTOP_PORTAL_DIR=../local/share/xdg-desktop-portal/portals GSETTINGS_SCHEMA_DIR=../local/share/glib-2.0/schemas/ LD_LIBRARY_PATH=/mnt/work/work/gnome/global_shortcuts/local/lib64/ ../local/bin/gnome-shell --nested --sm-disable
```

You can later use the dbus address for running all other programs:

```
DBUS_SESSION_BUS_ADDRESS=`cat ./local/DBUSA`
```

Install `gnome-globalshortcuts.portal` in `$XDG_DATA_DIRS/xdg-desktop-portal/portals/`, e.g. `/usr/share/xdg-desktop-portal/portals/`. In the same directory, modify the file `portals.conf` to say:

```
[preferred]
default=gnome
org.freedesktop.impl.portal.GlobalShortcuts=gnome-globalshortcuts
```

This will allow xdg-desktop-portal to call this impl. Run it like this:

```
XDG_DESKTOP_PORTAL_DIR=$XDG_DATA_DIRS/xdg-desktop-portal/portals/ /usr/libexec/xdg-desktop-portal --verbose --replace
```

Now you can start gsimpl:

```
python3 ./gsimpl.py
```

It will now respond to applications making portal requests. By failing, because it needs Settings to provide the user interface.

```
GSETTINGS_SCHEMA_DIR=../local/share/glib-2.0/schemas/ XDG_CURRENT_DESKTOP=GNOME ../g-c-c/globalshortcuts-provider/gnome-control-center-globalshortcuts-provider
```

Now run an application that uses the portal. Or a special test tool:

```
git clone https://gitlab.gnome.org/dcz/portal-poker
cd portal-poker
git checkout optionallll
PORTAL_FILE=../gnome-control-center/globalshortcuts-provider/org.gnome.GlobalshortcutsProvider.xml ./portal-poker.py portal GccShortcuts BindShortcuts org.kde.ark a b 'ctrl+c'
```

To modify a shortcut and see a different UI, run:

```
GSETTINGS_SCHEMA_DIR=../local/share/glib-2.0/schemas/ XDG_CURRENT_DESKTOP=GNOME ./shell/gnome-control-center
```